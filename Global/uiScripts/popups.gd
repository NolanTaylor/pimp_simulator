extends Control

const POPUP_POS : Rect2 = Rect2(87, 32, 224, 160)

signal condoms_a(paying)
signal contraceptives_a(paying)
signal std_screening_a(paying)
signal security_a(paying)

signal update_intercourse_cut(new_cut)
signal update_intercourse_price(new_price)
signal update_oral_sex_cut(new_cut)
signal update_oral_sex_price(new_price)
signal update_anal_cut(new_cut)
signal update_anal_price(new_price)

func _ready():
	pass
	
func _on_condoms_pressed():
	$condoms_popup.popup(POPUP_POS)
	
func _on_condoms_x_pressed():
	$condoms_popup.hide()
	
func _on_condom_a_toggled(button_pressed):
	emit_signal("condoms_a", button_pressed)
	
func _on_contraceptives_pressed():
	$contraceptives_popup.popup(POPUP_POS)
	
func _on_contraceptives_x_pressed():
	$contraceptives_popup.hide()
	
func _on_contraceptives_a_toggled(button_pressed):
	emit_signal("contraceptives_a", button_pressed)
	
func _on_std_screening_pressed():
	$std_screening_popup.popup(POPUP_POS)
	
func _on_std_screening_x_pressed():
	$std_screening_popup.hide()
	
func _on_std_screening_a_toggled(button_pressed):
	emit_signal("std_screening_a", button_pressed)
	
func _on_security_pressed():
	$security_popup.popup(POPUP_POS)
	
func _on_security_x_pressed():
	$security_popup.hide()
	
func _on_security_a_toggled(button_pressed):
	emit_signal("security_a", button_pressed)
	
func _on_intercourse_pressed():
	$intercourse_popup.popup(POPUP_POS)
	
func _on_intercourse_x_pressed():
	$intercourse_popup.hide()
	
func _on_intercourse_cut_lineEdit_text_changed(new_text):
	emit_signal("update_intercourse_cut", new_text)
	
func _on_intercourse_price_lineEdit_text_changed(new_text):
	emit_signal("update_intercourse_price", new_text)
	
func _on_oral_sex_pressed():
	$oral_sex_popup.popup(POPUP_POS)
	
func _on_oral_sex_x_pressed():
	$oral_sex_popup.hide()
	
func _on_oral_sex_cut_lineEdit_text_changed(new_text):
	emit_signal("update_oral_sex_cut", new_text)
	
func _on_oral_sex_price_lineEdit_text_changed(new_text):
	emit_signal("update_oral_sex_price", new_text)
	
func _on_anal_pressed():
	$anal_popup.popup(POPUP_POS)
	
func _on_anal_x_pressed():
	$anal_popup.hide()
	
func _on_anal_cut_lineEdit_text_changed(new_text):
	emit_signal("update_anal_cut", new_text)
	
func _on_anal_price_lineEdit_text_changed(new_text):
	emit_signal("update_anal_price", new_text)
