extends Control

func _ready():
	pass
	
func hide_all() -> void:
	for child in get_children():
		child.hide()
