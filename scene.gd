extends Node2D

const CONDOM_COST : float = 0.50

var condoms : bool = false
var contraceptives : bool = false
var std_screening : bool = false
var security : bool = false

var intercourse_cut : float
var intercourse_price : float
var intercourse_cost : float

var oral_sex_cut : float
var oral_sex_price : float
var oral_sex_cost : float

var anal_cut : float
var anal_price : float
var anal_cost : float

var week : int = 0
var day : int = 0
var hour : int = 0

func get_cost_label(popup : String):
	var str1 : String = "control/popups/"
	var str2 : String = "_popup/info_panel/cost_panel/total_cost"
	
	return get_node(str1 + popup + str2)
	
func get_profit_label(popup : String):
	var str1 : String = "control/popups/"
	var str2 : String = "_popup/info_panel/profit_panel/profit"
	
	return get_node(str1 + popup + str2)
	
func get_day() -> String:
	match day:
		1:
			return "Monday"
		2:
			return "Tuesday"
		3:
			return "Wednesday"
		4:
			return "Thursday"
		5:
			return "Friday"
		6:
			return "Saturday"
		7:
			return "Sunday"
		_:
			return "NULL"
	
func _ready():
	$timer.one_shot = true
	
	# risk management
	$control/popups.connect("condoms_a", self, \
	"condoms_a")
	$control/popups.connect("contraceptives_a", self, \
	"contraceptives_a")
	$control/popups.connect("std_screening_a", self, \
	"std_screening_a")
	$control/popups.connect("security_a", self, \
	"security_a")
	
	# intercourse
	$control/popups.connect("update_intercourse_cut", \
	self, "update_intercourse_cut")
	$control/popups.connect("update_intercourse_price", \
	self, "update_intercourse_price")
	$control/popups/intercourse_popup.connect("about_to_show", \
	self, "update_intercourse_cost")
	
	# oral sex
	$control/popups.connect("update_oral_sex_cut", \
	self, "update_oral_sex_cut")
	$control/popups.connect("update_oral_sex_price", \
	self, "update_oral_sex_price")
	$control/popups/oral_sex_popup.connect("about_to_show", \
	self, "update_oral_sex_cost")
	
	# anal
	$control/popups.connect("update_anal_cut", \
	self, "update_anal_cut")
	$control/popups.connect("update_anal_price", \
	self, "update_anal_price")
	$control/popups/anal_popup.connect("about_to_show", \
	self, "update_anal_cost")
	
func _process(delta):
	if $timer.is_stopped():
		$timer.start(10.0)
		hour += 1
		if hour > 24:
			hour = 0
			day += 1
			if day > 7:
				day = 1
				week += 1
		$control/hContainer/parent_panel/time.text = \
		"Week " + str(week) + "/" + get_day() + "/" + \
		str(hour) + "h"
		
		for john in $johns.johns[week]:
			if john["day"] == day and john["time"] == hour:
				satisfy_john(john)
				
# /* ------------------ john function ------------------ */
	
func satisfy_john(john : Dictionary) -> void:
	print(john)
	
# /* ----------------- risk management ----------------- */
	
func condoms_a(paying : bool) -> void:
	if paying:
		condoms = true
	else:
		condoms = false
	
func contraceptives_a(paying : bool) -> void:
	if paying:
		contraceptives = true
	else:
		contraceptives = false
		
func std_screening_a(paying : bool) -> void:
	if paying:
		std_screening = true
	else:
		std_screening = false
		
func security_a(paying : bool) -> void:
	if paying:
		security = true
	else:
		security = false
		
# /* -------------------- services -------------------- */
	
# intercourse
		
func update_intercourse_cut(cut : String) -> void:
	intercourse_cut = float(cut)
	update_intercourse_cost()
	
func update_intercourse_price(price : String) -> void:
	intercourse_price = float(price)
	update_intercourse_cost()
	
func update_intercourse_cost() -> void:
	var new_cost : float
	var new_profit : float
	new_cost = intercourse_price * (1.0 - (intercourse_cut / 100))
	if condoms:
		new_cost += CONDOM_COST
	new_profit = intercourse_price - new_cost
	intercourse_cost = new_cost
	get_cost_label("intercourse").text = \
	"total cost: $" + str(new_cost)
	get_profit_label("intercourse").text = \
	"profit: $" + str(new_profit)
	
# oral sex
	
func update_oral_sex_cut(cut : String) -> void:
	oral_sex_cut = float(cut)
	update_oral_sex_cost()
	
func update_oral_sex_price(price : String) -> void:
	oral_sex_price = float(price)
	update_oral_sex_cost()
	
func update_oral_sex_cost() -> void:
	var new_cost : float
	var new_profit : float
	new_cost = oral_sex_price * (1.0 - (oral_sex_cut / 100))
	if condoms:
		new_cost += CONDOM_COST
	new_profit = oral_sex_price - new_cost
	oral_sex_cost = new_cost
	get_cost_label("oral_sex").text = \
	"total cost: $" + str(new_cost)
	get_profit_label("oral_sex").text = \
	"profit: $" + str(new_profit)
	
# anal

func update_anal_cut(cut : String) -> void:
	anal_cut = float(cut)
	update_anal_cost()
	
func update_anal_price(price : String) -> void:
	anal_price = float(price)
	update_anal_cost()
	
func update_anal_cost() -> void:
	var new_cost : float
	var new_profit : float
	new_cost = anal_price * (1.0 - (anal_cut / 100))
	if condoms:
		new_cost += CONDOM_COST
	new_profit = anal_price - new_cost
	anal_cost = new_cost
	get_cost_label("anal").text = \
	"total cost: $" + str(new_cost)
	get_profit_label("anal").text = \
	"profit: $" + str(new_profit)
